
var canvas = document.getElementById('drawIt');
var con = canvas.getContext('2d');

var painting = document.getElementById('drawIt');
var paint_style = getComputedStyle(painting);

canvas.width = parseInt(paint_style.getPropertyValue('width'));
canvas.height = parseInt(paint_style.getPropertyValue('height'));

var mouse = {x: 0, y: 0};

canvas.addEventListener('mousemove', function(e) {
    mouse.x = e.pageX - this.offsetLeft;
    mouse.y = e.pageY - this.offsetTop;

}, false);

con.lineWidth = 4;
con.lineJoin - 'round';
con.lineCap = 'round';
con.strokeStyle = '#FF0DFF';

canvas.addEventListener('mousedown', function(e) {
    con.beginPath();
    con.moveTo(mouse.x, mouse.y);

    canvas.addEventListener('mousemove', onPaint, false);
}, false);

canvas.addEventListener('mouseup', function(){
    canvas.removeEventListener('mousemove', onPaint,false);

}, false);

var onPaint = function(){
    con.lineTo(mouse.x, mouse.y);
    con.stroke();

};
