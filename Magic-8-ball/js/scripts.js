//Global variables
//create variables for all DOM objects that will be part of the
//application interactions
var code;
var random;
var msg;

$(function() {
    $("magic8ball").click(reset8Ball);
    $('form').submit(function(e) {
        e.preventDefault();

        if($("#question").val() == ''){
            showAnswer("You need to ask a question");
        }
        else{
            getFortune();

        }
    });
});

//Helper functions
var getRandomIndex = function(max) {

    code = Math.random();

};


var showAnswer = function(msg) {

    $('#answer').html(msg).delay('700').fadeIn('fast');
    $('img').delay('700').fadeOut('fast');
};


var getFortune = function() {
   getRandomIndex();
    $('#magic8ball').effect("shake", {times:2,direction:"up"}, 100);
    $.ajax({
        url:'php/fortune.php',

        type:'post',

        data: {"code":code},

        success: function(response) {
            showAnswer(response);
        },

        error: function(xhr) {
            showAnswer('u gotta write something idiot');
        },
    });
};

var reset8Ball = function () {
    $('#magic8ball').effect("shake", {times: 1, direction: "up"}, 100);
    $('#answer').html('').fadeOut('fast');
    $('img').fadeIn('fast');
};
