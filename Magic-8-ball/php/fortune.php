<?php
$msg = array("It is certain","It is decidedly so",
"Without a doubt","Yes, definitely","You may rely on it","As I see it, yes",
"Most likely","Outlook good","Yes","Signs point to yes","Reply hazy try again",
"Ask again later","Better not tell you now","Cannot predict now","Concentrate and ask again
","Don't count on it","My reply is no","My sources say no","Outlook not so good","Very doubtful");
//Create an array of strings to store the fortunes

//Shuffle the array
Shuffle($msg);
//Use a value passed into this script to calculate
//a random integer that is a valid index in the
//fortunes array
$index =  intval($_POST['code']) % count($msg);
echo $msg[$index];
//Use the 'echo' command to print the fortune
//located at the random index value in the array

?>
